<?php 

	echo "<h1>Connection to the database</h1>";

	// The information can be found in the "mysql" service of the "docker-compose.yml" file at the root of the project.

	$db_host = "mysql";
	$db_name = "webdb";
	$db_user = "user";
	$db_password = "user123";
	$dsn = "mysql:host=$db_host;dbname=$db_name;charset=utf8";

	try
	{
		new PDO($dsn, $db_user, $db_password);
		echo "<p><span style='color:green'>SUCCESS</span></p>";
	}
	catch (Exception $e)
	{
		echo "<p><span style='color:red'>ERROR</span> <span style='color:black'>".$e->getMessage()."</span></p>";
		die();
	}
?>