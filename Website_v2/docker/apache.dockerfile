FROM httpd:2.4.43-alpine

RUN apk update && apk upgrade

COPY apache/config.apache.conf /usr/local/apache2/conf/config.apache.conf

RUN sed -i -e '/Listen 80/cListen 8080' /usr/local/apache2/conf/httpd.conf
RUN echo "Include /usr/local/apache2/conf/config.apache.conf" >> /usr/local/apache2/conf/httpd.conf
