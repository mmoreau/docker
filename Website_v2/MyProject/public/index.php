<?php 
	require_once "../lib/Person.php";
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Index</title>
	</head>
	<body>
		<h1>Index</h1>
		<a href="db.php">Test DataBase</a><br/><br/>

		<?php 

			$persons = [
				new Person("Jimi", "Hendrix"),
				new Person("Keith", "Richards"),
				new Person("Bill", "Gates"),
				new Person("Steve", "Jobs")
			];

			$persons[0]->setFirstName("Johnny Allen");

			foreach ($persons as $person)
			{
				echo $person->getFirstName().' '.$person->getLastName()."<br/>";
			}

			
			/*
			echo "<pre>";
			var_dump($persons);
			echo "</pre>";
			*/
		?>
	</body>
</html>